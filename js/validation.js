
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab);

var stop = false;
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function validatePhone(pnum) {
    var pn = /^(09|9|\+98([\s]{1,3})?9)[0123][0-9]\d{7}$/
    return pn.test(pnum);
}
// function validatePhoneNumber(pnumber){
//     var pn = /^(09|9|\+989)[1][0-9]\d{7}$)|(^(09|9|\+989)[023][12456789]\d{7}$/;

// }
function showTab(n) {
    // This function will display the specified tab of the form ...
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    // ... and fix the Previous/Next buttons:
    // if (n == 0) {
    //     document.getElementById("prevBtn").style.display = "none";
    // } else {
    //     document.getElementById("prevBtn").style.display = "inline";
    // }
    if (n == 1) {
        document.getElementById("mobile-num").innerHTML = document.getElementById("phone_email").value;
        document.getElementById("txt1").focus();
        stop = false;
        document.getElementById('timer').innerHTML = 002 + ":" + 00;
        startTimer();

    }
    if (n == (x.length - 1)) {
        document.getElementById("next-prev-btn").innerHTML = "ثبت و تایید کد ارسال شده";
    } else {
        document.getElementById("next-prev-btn").innerHTML = "ثبت";
    }
    // ... and run a function that displays the correct step indicator:
    // fixStepIndicator(n)
}
function nextPrev(n) {
    // This function will figure out which tab to display
   
    var x = document.getElementsByClassName("tab");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm()) {
        return false;
    }
    document.getElementsByClassName("login-input")[0].classList.remove("invalid");
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form... :
    if (currentTab >= x.length) {
        //...the form gets submitted:
        document.getElementById("reg-form").submit();
        return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
    // check the edit phone number btn was clicked
    if (currentTab == 0 && n == -1) {
        // stop timer
        stop = true;
        document.getElementById("phone_email").value = "";
        document.getElementById("phone_email").required = true;
        document.getElementById("phone_email").focus();
        // clear the values of verification codes
        digits = document.getElementsByClassName("digit-val");
        for (var index = 0; index < digits.length; index++) {
            digits[index].classList.remove("invalid");
            digits[index].value = "";
            
        }
    }
}
function validateForm() {
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");
    // A loop that checks every input field in the current tab:
    if (currentTab == 0) {
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "" || !(validatePhone(y[i].value) || validateEmail(y[i].value))) {
                // add an "invalid" class to the field:
                if (!(y[i].classList.contains("invalid"))){
                    y[i].className += " invalid";
                    // y[i].nextSibling.style.display = "block";
                    y[i].nextElementSibling.innerHTML = "لطفا شماره موبایل یا ایمیل را به فرمت صحیح وارد کنید";     
                }
                // and set the current valid status to false:
                valid = false;
                

            }else{
                y[i].nextElementSibling.innerHTML = "";
            }
        }
    }
    else {
        if (currentTab == 1) {
            if (!checkNumbers(0)) {
                valid = false;

            }
        }
    }

    // If the valid status is true, mark the step as finished and valid:
    // if (valid) {
    //     document.getElementsByClassName("step")[currentTab].className += " finish";
    // }

    return valid; // return the valid status
}
function moveCursor(from, to) {
    length = from.value.length;
    if (length == 1) {
        document.getElementById(to).focus();
    }
}
// function fixStepIndicator(n) {
//     // This function removes the "active" class of all steps...
//     var i, x = document.getElementsByClassName("step");
//     for (i = 0; i < x.length; i++) {
//         x[i].className = x[i].className.replace(" active", "");
//     }
//     //... and adds the "active" class to the current step:
//     x[n].className += " active";
// }

function startTimer() {

    var presentTime = document.getElementById('timer').innerHTML;
    var timeArray = presentTime.split(/[:]+/);
    var m = timeArray[0];
    var s = checkSecond((timeArray[1] - 1));
    if (s == 59) { m = m - 1 }
    //if(m<0){alert('timer completed')}

    document.getElementById('timer').innerHTML =
        m + ":" + s;
    // console.log(m)
    if (stop == false) {
        setTimeout(startTimer, 1000);
    } else {
        return false;
    }

}

function checkSecond(sec) {
    if (sec < 10 && sec >= 0) { sec = "0" + sec }; // add zero in front of numbers < 10
    if (sec < 0) { sec = "59" };
    return sec;
}


function checkNumbers(btn_txt) {
    var txt_digits = document.getElementById("digit-code").getElementsByClassName("digit-val");
    var num = "";
    for (var index = 0; index < txt_digits.length; index++) {
        num = num + txt_digits[index].value;
    }
    if (parseInt(num) == 12345) {
        if (btn_txt == 1) {
            for (var index = 0; index < txt_digits.length; index++) {
                txt_digits[index].classList.remove("invalid");
            }
            nextPrev(1);

        }
        else {
            return true;
        }
    }
    for (var index = 0; index < txt_digits.length; index++) {
        txt_digits[index].className += " invalid";
    }
    return false;
    // else {

    //     alert("please insert correcr code the correct number is :" + num);
    // }
}

